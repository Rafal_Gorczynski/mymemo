
import Foundation
import AudioToolbox
import AVFoundation

// działa tylko na krótkich dzwiękach (do 30 sec)
func playClickSound() {
    if let soundUrl = Bundle.main.url(forResource: "click", withExtension: "mp3") {
        var soundId: SystemSoundID = 0
        
        AudioServicesCreateSystemSoundID(soundUrl as CFURL, &soundId)
        
        AudioServicesAddSystemSoundCompletion(soundId, nil, nil, { (soundId, clientData) -> Void in
            AudioServicesDisposeSystemSoundID(soundId)
        }, nil)
        
        AudioServicesPlaySystemSound(soundId)
    }
}

fileprivate var themeMusic: AVAudioPlayer!
func playThemeMusic() {
    let path = Bundle.main.path(forResource: "themeMusic.mp3", ofType:nil)!
    let url = URL(fileURLWithPath: path)
    
    do {
        let sound = try AVAudioPlayer(contentsOf: url)
        themeMusic = sound
        sound.setVolume(0.25, fadeDuration: 3.0)
        sound.numberOfLoops = -1
        sound.play()
    } catch {
    }
}
