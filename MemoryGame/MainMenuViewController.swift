
import UIKit

class MainMenuViewController: UIViewController {
    
    @IBAction func easyDifficultyAction(_ sender: Any) {
        performSegue(withIdentifier: "easyDifficultySegue", sender: self)
    }
    
    @IBAction func mediumDifficultyAction(_ sender: Any) {
        performSegue(withIdentifier: "mediumDifficultySegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        playThemeMusic()
    }
}
