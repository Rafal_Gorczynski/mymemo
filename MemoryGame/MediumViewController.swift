
import UIKit

// ogarnąć fx vo zwraca tylko wytyczony zakres indexów, zrobić jeden wspólny array dla wszystkich kart i połowa z tego kodu stanie się zbędna

class MediumViewController: ViewController {
    
    //da się overridować IBOutlety pomiędzu viewControllerami
    @IBOutlet weak var mediumTimerLabel: UILabel!
    @IBOutlet var mediumCardsCollection: [UIButton]!
    @IBAction func backToMainMenuAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction override func pickCard(_ sender: UIButton){
        let tag = sender.tag
        if selectedCards.count < 2 {
            selectCards(card: shuffledCards[tag], cardButton: mediumCardsCollection[tag], shuffledCardsIndex: tag)
        }
        playClickSound()
        gameOver()
    }
    @IBAction func startMediumButtonAction(_ sender: Any) {
        self.hideOrShowCards(isHidden: false)
        self.shuffledCards = DataManager.shared.mediumShuffledCard
        self.changeCardsImages(image: #imageLiteral(resourceName: "back"))
        startTimer()

    }
    
    @IBAction override func startButtonAction(_ sender: Any) {
        self.hideOrShowCards(isHidden: false)
        self.shuffledCards = DataManager.shared.mediumShuffledCard
        self.changeCardsImages(image: #imageLiteral(resourceName: "back"))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"background")!)
        hideOrShowCards(isHidden: true)
    }
    
    override func gameOver() {
        for card in mediumCardsCollection {
            if card.isHidden == false {
                cardButtonsAreHidden = false
            } else {
                cardButtonsAreHidden = true
            }
        }
        if cardButtonsAreHidden == true {
            timer.invalidate()
            print ("game over")
        }
    }
    
    override func timePassed() {
        time += 1
        mediumTimerLabel.text = String(time)
    }

    
    override func changeCardsImages (image: UIImage?){
        mediumCardsCollection.forEach { $0.setImage(image, for: .normal)
        }
    }

    override func hideOrShowCards(isHidden: Bool) {
        mediumCardsCollection.forEach { $0.isHidden = isHidden
        }
    }
    
    

    
}
