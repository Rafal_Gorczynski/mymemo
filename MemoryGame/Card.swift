
import Foundation
import UIKit

class Card {
    let number: Int
    let image: UIImage
    
    init (number: Int, image: UIImage){
        self.number = number
        self.image = image
    }
}
