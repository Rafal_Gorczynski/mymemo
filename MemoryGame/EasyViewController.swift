
import UIKit

class ViewController: UIViewController {
    //dodac punkty, najszybszy czas przejscia (hi-scores) i jakis komunikat, że wygrałeś. Ew warunek, że masz x sekund na przjscie, inaczej gameover i jakieś ładne animacje
    var shuffledCards = [Card]()
    var selectedCards = [Card]()
    var selectedCardsButtons = [UIButton]()
    var timer = Timer()
    var time = 0
    var cardButtonsAreHidden = false
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet var cardsCollection: [UIButton]!
    @IBAction func pickCard(_ sender: UIButton){
        let tag = sender.tag
        if selectedCards.count < 2 {
            selectCards(card: shuffledCards[tag], cardButton: cardsCollection[tag], shuffledCardsIndex: tag)
        }
        gameOver()
        playClickSound()
    }
    
    @IBAction func backToMainMenuAction2(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func startButtonAction(_ sender: Any) {
        cardButtonsAreHidden = false
        hideOrShowCards(isHidden: false)
        shuffledCards = DataManager.shared.shuffledCard
        changeCardsImages(image: #imageLiteral(resourceName: "back"))
        startTimer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"background")!)
        hideOrShowCards(isHidden: true)
    }
    
    func gameOver() {
        cardsCollection.forEach {
            if $0.isHidden == false {
                cardButtonsAreHidden = false
            } else {
                cardButtonsAreHidden = true
            }
        }
        if cardButtonsAreHidden == true {
            timer.invalidate()
            DataManager.shared.easyHiScores.append(time)
            print (DataManager.shared.easyHiScores)
        }
    }
    
    func startTimer() {
        timer.invalidate()
        time = 0
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timePassed), userInfo: nil, repeats: true)
    }
    
    func timePassed() {
        time += 1
        timerLabel.text = String(time)
    }
    
    func changeCardsImages (image: UIImage?){
        cardsCollection.forEach { $0.setImage(image, for: .normal)
        }
    }
    
    func hideOrShowCards(isHidden: Bool) {
        cardsCollection.forEach { $0.isHidden = isHidden
        }
    }
    
    func selectCards(card: Card, cardButton: UIButton, shuffledCardsIndex: Int) {
        cardButton.setImage(shuffledCards[shuffledCardsIndex].image, for: .normal)
        selectedCards.append(card)
        selectedCardsButtons.append(cardButton)
        
        if selectedCards.count >= 2 && selectedCardsButtons.count >= 2 {
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (timer) in
                self.changeCardsImages(image: #imageLiteral(resourceName: "back"))
            }
            if selectedCards.first?.number == selectedCards.last?.number && selectedCardsButtons.first != selectedCardsButtons.last {
                selectedCardsButtons.first?.isHidden = true
                selectedCardsButtons.last?.isHidden = true
            }
            self.selectedCards.removeAll()
            self.selectedCardsButtons.removeAll()
        }
    }
}

