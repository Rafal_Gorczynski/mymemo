
import Foundation
import UIKit

class DataManager {
    static let shared = DataManager()
    var shuffledCard: [Card] {
        return easyCards.shuffled()
    }
    var mediumShuffledCard: [Card] {
        return mediumCards.shuffled()
    }
    var easyHiScores = [Int]()
    var mediumHiScores = [Int]()
    
    fileprivate let easyCards = [
        Card(number: 1, image: #imageLiteral(resourceName: "ace")),
        Card(number: 1, image: #imageLiteral(resourceName: "ace")),
        Card(number: 2, image: #imageLiteral(resourceName: "card2")),
        Card(number: 2, image: #imageLiteral(resourceName: "card2")),
        Card(number: 3, image: #imageLiteral(resourceName: "card3")),
        Card(number: 3, image: #imageLiteral(resourceName: "card3"))]
    
    fileprivate let mediumCards = [
        Card(number: 1, image: #imageLiteral(resourceName: "ace")),
        Card(number: 1, image: #imageLiteral(resourceName: "ace")),
        Card(number: 2, image: #imageLiteral(resourceName: "card2")),
        Card(number: 2, image: #imageLiteral(resourceName: "card2")),
        Card(number: 3, image: #imageLiteral(resourceName: "card3")),
        Card(number: 3, image: #imageLiteral(resourceName: "card3")),
        Card(number: 4, image: #imageLiteral(resourceName: "card4")),
        Card(number: 4, image: #imageLiteral(resourceName: "card4")),
        Card(number: 5, image: #imageLiteral(resourceName: "card5")),
        Card(number: 5, image: #imageLiteral(resourceName: "card5")),
        Card(number: 6, image: #imageLiteral(resourceName: "card6")),
        Card(number: 6, image: #imageLiteral(resourceName: "card6")),
        Card(number: 7, image: #imageLiteral(resourceName: "card7")),
        Card(number: 7, image: #imageLiteral(resourceName: "card7")),
        Card(number: 8, image: #imageLiteral(resourceName: "card8")),
        Card(number: 8, image: #imageLiteral(resourceName: "card8"))]

}
